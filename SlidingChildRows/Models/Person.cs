﻿namespace SlidingChildRows.Models
{
    public class Person
    {
        public string Name { get; set; }

        public string BirthYear { get; set; }

        public string HairColor { get; set; }
    }
}