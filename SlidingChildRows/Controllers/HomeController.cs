﻿using System.Linq;
using System.Web.Mvc;
using SharpTrooper.Core;
using SharpTrooper.Entities;
using SlidingChildRows.Models;

namespace SlidingChildRows.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Planets(int draw)
        {
            var core = new SharpTrooperCore();

            var planets = core.GetAllPlanets();

            return Json(new
            {
                draw,
                recordsTotal = planets.count,
                recordsFiltered = 0,
                data = planets.results.Select(
                    p => new {p.name, p.terrain, p.url, p.residents})
            });
        }

        
        public ActionResult People(string planetUrl)
        {
            var core = new SharpTrooperCore();

            var peopleUrl = core.GetSingleByUrl<Planet>(planetUrl).residents;

            var people = peopleUrl.Select(p =>
            {
                var person = core.GetSingleByUrl<People>(p);

                return new Person {Name = person.name, BirthYear = person.birth_year, HairColor = person.hair_color};
            }).ToList();


            return PartialView("_Person", people);
        }
    }
}